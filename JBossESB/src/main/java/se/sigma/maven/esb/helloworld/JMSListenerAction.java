package se.sigma.maven.esb.helloworld;

import org.jboss.soa.esb.actions.AbstractActionLifecycle;
import org.jboss.soa.esb.helpers.ConfigTree;
import org.jboss.soa.esb.message.Message;

public class JMSListenerAction extends AbstractActionLifecycle {
    protected ConfigTree config;

    public JMSListenerAction(ConfigTree config) {
        this.config = config;
    }

    public Message displayMessage(Message message) throws Exception {
        System.out.println("================================================");
        System.out.println("Body: " + message.getBody().get());
        System.out.println("&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&");

        return message;
    }
}
